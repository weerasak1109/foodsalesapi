﻿using FoodSalesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodSalesAPI.Utilitys
{
    public class ErrorUtility<TValue>
    {
        /// <summary>
        /// Get success message
        /// </summary>
        /// <returns></returns>
        public static OutputModels<TValue> GetSuccessMessage(int error_code, int sub_code, TValue data, string message = "สำเร็จ")
        {
            var output = new OutputModels<TValue>()
            {
                data = data,
                error_code = error_code,
                sub_code = sub_code,
                message = message,
                title = "แจ้งเตือน"
            };
            return output;
        }

        /// <summary>
        /// Get error message
        /// </summary>
        /// <returns></returns>
        public static OutputModels<TValue> GetErrorMessage(int error_code, int sub_code, string title = Title.Exception, string message = "")
        {
            try
            {
                var output = new OutputModels<TValue>()
                {
                    error_code = error_code,
                    sub_code = sub_code,
                    title = title,
                    message = message
                };

                return output;
            }
            catch (Exception ex)
            {
                var output = new OutputModels<TValue>()
                {
                    error_code = error_code,
                    sub_code = sub_code,
                    title = title,
                    message = ex.Message
                };
                return output;
            }
        }

    }
}