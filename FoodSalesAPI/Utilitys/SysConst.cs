﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodSalesAPI.Utilitys
{
    public class ErrorCode
    {
        public const int UNKNOWN = 0;
        public const int SUCCESS = 1;
        public const int ERROR = -1;
    }
    public class SubCode
    {
        public const int SUCCESS = 1;
        public const int DATA_NOT_FOUND = -1;
        public const int INSERT_DATA_ERROR = -2;
        public const int DELETE_ERROR = -3;
        public const int INTERNAL_ERROR = -1000;
        public const int DATA_INPUT_ERROR = -1001;
    }

    public class Title
    {
        public const string Exception = "ข้อผิดพลาด";
    }

    public class Message
    {
        public const string DATA_NOT_FOUND = "ไม่พบข้อมูล";
        public const string ERROR = "ข้อผิดพลาด";
        public const string INSERT_DATA_ERROR = "บันทึกไม่สำเร็จ";
        public const string DELETE_ERROR = "ลบข้อมูลไม่สำเร็จ";
        public const string DATA_INPUT_ERROR = "ข้อผิดพลาด Data Input";
    }
}