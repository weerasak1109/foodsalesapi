﻿using FoodSalesAPI.Management;
using FoodSalesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace FoodSalesAPI.Controllers
{
    public class RegionController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("region/by/{name}")]
        public OutputModels<z_region> GetRegionByName(string name)
        {
            try
            {
                return RegionManagement.I.RegionByName(name);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("region/list")]
        public OutputModels<List<z_region>> GetRegionList()
        {
            try
            {
                return RegionManagement.I.GetRegionList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}