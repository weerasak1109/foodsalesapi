﻿using FoodSalesAPI.Management;
using FoodSalesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace FoodSalesAPI.Controllers
{
    public class CityController : ApiController
    {
        /// <summary>
        ///  
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("city/by/{name}")]
        public OutputModels<z_city> GetCityByName(string name)
        {
            try
            {
                return CityManagement.I.CityByName(name);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("city/list")]
        public OutputModels<List<z_city>> GetCityList()
        {
            try
            {
                return CityManagement.I.GetCityList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}