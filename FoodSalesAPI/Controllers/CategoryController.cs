﻿using FoodSalesAPI.Management;
using FoodSalesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace FoodSalesAPI.Controllers
{
    public class CategoryController : ApiController
    {
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("category/by/{name}")]
        public OutputModels<z_category> GetCategoryByName(string name)
        {
            try
            {
                return CategoryManagement.I.CategoryByName(name);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("category/list")]
        public OutputModels<List<z_category>> GetCategoryList()
        {
            try
            {
                return CategoryManagement.I.GetCategoryList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}