﻿using FoodSalesAPI.Management;
using FoodSalesAPI.Models;
using FoodSalesAPI.Services;
using Microsoft.Ajax.Utilities;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace FoodSalesAPI.Controllers
{
    public class FoodController : ApiController
    {
        //IHttpActionResult
        //return Json(FoodManagement.I.GetFoodList());

        /// <summary>
        /// Get Food By Id
        /// </summary>
        /// <param name="id">รหัสสินค้า</param>
        /// <returns></returns>
        [HttpGet]
        [Route("food/by/{id}")]
        public OutputModels<m_food> GetFoodById(int id)
        {
            try
            {
                return FoodManagement.I.FoodById(id);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Food List
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("food/list")]
        public OutputModels<List<m_food>> GetFoodList()
        {
            try
            {
                return FoodManagement.I.GetFoodList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// delete Food By Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="inactive"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("food/delete/{id}/{inactive}")]
        public OutputModels<bool> deleteFoodById(int id, bool inactive = false)
        {
            try
            {
                return FoodManagement.I.deleteFoodById(id, inactive);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("food/add")]
        public OutputModels<bool> PostFoodAdd(FoodModelsIn req)
        {
            try
            {
                return FoodManagement.I.FoodAdd(req);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("food/edit")]
        public OutputModels<bool> PostFoodEdit(FoodeditModelsIn req)
        {
            try
            {
                return FoodManagement.I.FoodEdit(req);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("food/Search")]
        public OutputModels<List<m_food>> PostFoodSearch(FoodSearchModelsIn req)
        {
            try
            {
                return FoodManagement.I.PostFoodSearch(req);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}