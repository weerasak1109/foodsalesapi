﻿using FoodSalesAPI.Management;
using FoodSalesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace FoodSalesAPI.Controllers
{
    public class DataMasterController : ApiController   
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("data/master/set")]
        public IHttpActionResult DataMasterSet()
        {
            try
            {
                return Json(DataMasterManagement.I.DataMasterSet());
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}