﻿using ExcelDataReader;
using FoodSalesAPI.DBConnector;
using FoodSalesAPI.Models;
using FoodSalesAPI.Services;
using FoodSalesAPI.Utilitys;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using static System.Net.Mime.MediaTypeNames;

namespace FoodSalesAPI.Management
{
    public class DataMasterManagement
    {
        #region Singleton
        private FoodService _foodService;
        private RegionService _regionService;
        private CityService _cityService;
        private CategoryService _categoryService;
        private static readonly Lazy<DataMasterManagement> instance = new Lazy<DataMasterManagement>(() => Activator.CreateInstance<DataMasterManagement>());
        public static DataMasterManagement I
        {
            get
            {
                return instance.Value;
            }
        }
        public DataMasterManagement()
        {
        }
        #endregion


        public OutputModels<bool> DataMasterSet()
        {
            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;

                var iData = ReadExcelFile();
                if (iData.error_code == 1 && iData.sub_code == 1)
                {
                    //m_food
                    _foodService = new FoodService(conn);
                    var res = _foodService.InsertFood(iData.data);
                    if (res == false)
                    {
                        return ErrorUtility<bool>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.INSERT_DATA_ERROR, false, Message.INSERT_DATA_ERROR);
                    }

                    #region z_category
                    _categoryService = new CategoryService(conn);
                    var Dis_category = iData.data.Select(e => e.Category).Distinct().ToList();
                    List<z_category> z_Categories = new List<z_category>();
                    if (Dis_category != null && Dis_category.Count > 0)
                    {
                        foreach (var ii in Dis_category)
                        {
                            z_category z_Category = new z_category()
                            {
                                Category = ii
                            };
                            z_Categories.Add(z_Category);
                        }

                        _categoryService.InsertCategory(z_Categories);
                        if (res == false)
                        {
                            return ErrorUtility<bool>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.INSERT_DATA_ERROR, false, Message.INSERT_DATA_ERROR);
                        }
                    }

                    #endregion

                    #region z_region
                    _regionService = new RegionService(conn);
                    var Dis_region = iData.data.Select(e => e.Region).Distinct().ToList();
                    List<z_region> z_Regions = new List<z_region>();
                    if (Dis_region != null && Dis_region.Count > 0)
                    {
                        foreach (var ss in Dis_region)
                        {
                            z_region z_region = new z_region()
                            {
                                Region = ss
                            };
                            z_Regions.Add(z_region);
                        }
                        _regionService.InsertRegion(z_Regions);
                        if (res == false)
                        {
                            return ErrorUtility<bool>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.INSERT_DATA_ERROR, false, Message.INSERT_DATA_ERROR);
                        }
                    }
                    #endregion

                    #region z_city
                    _cityService = new CityService(conn);    
                    var Dis_city = iData.data.Select(e => e.City).Distinct().ToList();
                    List<z_city> z_city = new List<z_city>();
                    if (Dis_city != null && Dis_city.Count > 0)
                    {
                        foreach (var hh in Dis_city)
                        {
                            z_city citys = new z_city()
                            {
                                City = hh
                            };
                            z_city.Add(citys);
                        }
                        _cityService.InsertCity(z_city);
                        if (res == false)
                        {
                            return ErrorUtility<bool>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.INSERT_DATA_ERROR, false, Message.INSERT_DATA_ERROR);
                        }
                    }
                    #endregion

                    //test
                    //var t = _categoryService.CategoryAll();
                    //var t2 = _regionService.RegionAll();
                    //var t3 = _cityService.CityAll();


                    return ErrorUtility<bool>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.SUCCESS, true);
                }
                else
                {
                    return ErrorUtility<bool>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.DATA_NOT_FOUND, false, Message.ERROR);
                }
            }
            catch (Exception ex)
            {
                return ErrorUtility<bool>.GetErrorMessage(ErrorCode.ERROR, SubCode.INTERNAL_ERROR, Message.ERROR, ex.Message);
            }
        }



        public OutputModels<List<m_food>> ReadExcelFile()
        {
            List<m_food> m_Foods = null;
            DataSet ds = null;
            try
            {
                //var path1 = @"D:\GitHub\FoodSalesAPI\FoodSalesAPI\File\Food sales.xlsx";
                var path1 = System.Web.HttpContext.Current.Server.MapPath("~\\File\\Food sales.xlsx");
                using (var stream = new FileStream(path1, FileMode.Open, FileAccess.Read))
                {
                    using (IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream))
                    {
                        ds = reader.AsDataSet(new ExcelDataSetConfiguration()
                        {
                            ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                            {
                                UseHeaderRow = true
                            }
                        });
                    }
                }

                if (ds != null && ds.Tables != null)
                {
                    m_Foods = new List<m_food>();
                    List<food_ex> food_ex = ConvertToList<food_ex>(ds.Tables[0]);

                    if (food_ex != null && food_ex.Count > 0)
                    {
                        int g = 1;
                        foreach (var i in food_ex)
                        {
                            m_food m_Food = new m_food()
                            {
                                Quantity = Convert.ToInt32(i.Quantity),
                                Category = i.Category,
                                City = i.City,
                                foodID = g,
                                inactive = false,
                                OrderDate = i.OrderDate,
                                Product = i.Product,
                                Region = i.Region,
                                TotalPrice = i.TotalPrice,
                                UnitPrice = i.UnitPrice
                            };
                            m_Foods.Add(m_Food);
                            g++;
                        }
                    }
                    return ErrorUtility<List<m_food>>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.SUCCESS, m_Foods);
                }
                else
                {
                    return ErrorUtility<List<m_food>>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.DATA_NOT_FOUND, m_Foods, Message.DATA_NOT_FOUND);
                }
            }
            catch (Exception ex)
            {
                return ErrorUtility<List<m_food>>.GetErrorMessage(ErrorCode.ERROR, SubCode.INTERNAL_ERROR, Message.ERROR, ex.Message);
            }
        }


        public static List<T> ConvertToList<T>(DataTable dt)
        {
            var columnNames = dt.Columns.Cast<DataColumn>().Select(c => c.ColumnName.ToLower()).ToList();
            var properties = typeof(T).GetProperties();
            return dt.AsEnumerable().Select(row =>
            {
                var objT = Activator.CreateInstance<T>();
                foreach (var p in properties)
                {
                    if (columnNames.Contains(p.Name.ToLower()))
                    {
                        try
                        {
                            p.SetValue(objT, row[p.Name]);
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
                return objT;
            }).ToList();
        }
    }
}