﻿using FoodSalesAPI.DBConnector;
using FoodSalesAPI.Models;
using FoodSalesAPI.Services;
using FoodSalesAPI.Utilitys;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodSalesAPI.Management
{
    public class CategoryManagement    
    { 
        #region Singleton
        private CategoryService _service;
        private static readonly Lazy<CategoryManagement> instance = new Lazy<CategoryManagement>(() => Activator.CreateInstance<CategoryManagement>());
        public static CategoryManagement I
        {
            get
            {
                return instance.Value;
            }
        }
        public CategoryManagement()
        {
        }
        #endregion

        /// <summary>
        /// Category By Name Management
        /// </summary>
        /// <param name="iName"></param>
        /// <returns></returns>
        public OutputModels<z_category> CategoryByName(string iName)
        {
            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new CategoryService(conn);
                z_category data = _service.CategoryByName(iName);

                if (data != null)
                {
                    return ErrorUtility<z_category>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.SUCCESS, data);
                }
                else
                {
                    return ErrorUtility<z_category>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.DATA_NOT_FOUND, data, Message.DATA_NOT_FOUND);
                }
            }
            catch (Exception ex)
            {
                return ErrorUtility<z_category>.GetErrorMessage(ErrorCode.ERROR, SubCode.INTERNAL_ERROR, Message.ERROR, ex.Message);
            }

        }

        /// <summary>
        /// Get Category List Management
        /// </summary>
        /// <returns></returns>
        public OutputModels<List<z_category>> GetCategoryList()
        {
            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new CategoryService(conn);
                List<z_category> z_categorys = _service.CategoryAll();

                if (z_categorys != null)
                {
                    return ErrorUtility<List<z_category>>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.SUCCESS, z_categorys);
                }
                else
                {
                    return ErrorUtility<List<z_category>>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.DATA_NOT_FOUND, z_categorys, Message.DATA_NOT_FOUND);
                }
            }
            catch (Exception ex)
            {
                return ErrorUtility<List<z_category>>.GetErrorMessage(ErrorCode.ERROR, SubCode.INTERNAL_ERROR, Message.ERROR, ex.Message);
            }

        }
    }
}