﻿using FoodSalesAPI.DBConnector;
using FoodSalesAPI.Models;
using FoodSalesAPI.Services;
using FoodSalesAPI.Utilitys;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodSalesAPI.Management
{
    public class CityManagement  
    {
        #region Singleton
        private CityService _service;
        private static readonly Lazy<CityManagement> instance = new Lazy<CityManagement>(() => Activator.CreateInstance<CityManagement>());
        public static CityManagement I
        {
            get
            {
                return instance.Value;
            }
        }
        public CityManagement()
        {
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iName"></param>
        /// <returns></returns>
        public OutputModels<z_city> CityByName(string iName)
        {
            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new CityService(conn);
                z_city data = _service.CityByName(iName);

                if (data != null)
                {
                    return ErrorUtility<z_city>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.SUCCESS, data);
                }
                else
                {
                    return ErrorUtility<z_city>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.DATA_NOT_FOUND, data, Message.DATA_NOT_FOUND);
                }
            }
            catch (Exception ex)
            {
                return ErrorUtility<z_city>.GetErrorMessage(ErrorCode.ERROR, SubCode.INTERNAL_ERROR, Message.ERROR, ex.Message);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public OutputModels<List<z_city>> GetCityList()
        {
            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new CityService(conn);
                List<z_city> data = _service.CityAll();

                if (data != null)
                {
                    return ErrorUtility<List<z_city>>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.SUCCESS, data);
                }
                else
                {
                    return ErrorUtility<List<z_city>>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.DATA_NOT_FOUND, data, Message.DATA_NOT_FOUND);
                }
            }
            catch (Exception ex)
            {
                return ErrorUtility<List<z_city>>.GetErrorMessage(ErrorCode.ERROR, SubCode.INTERNAL_ERROR, Message.ERROR, ex.Message);
            }

        }
    }
}