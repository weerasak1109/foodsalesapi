﻿using FoodSalesAPI.DBConnector;
using FoodSalesAPI.Models;
using FoodSalesAPI.Services;
using FoodSalesAPI.Utilitys;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodSalesAPI.Management
{
    public class RegionManagement
    {
        #region Singleton
        private RegionService _service;
        private static readonly Lazy<RegionManagement> instance = new Lazy<RegionManagement>(() => Activator.CreateInstance<RegionManagement>());
        public static RegionManagement I
        {
            get
            {
                return instance.Value;
            }
        }
        public RegionManagement()
        {
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public OutputModels<z_region> RegionByName(string name)
        {
            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new RegionService(conn);
                z_region data = _service.RegionByName(name);

                if (data != null)
                {
                    return ErrorUtility<z_region>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.SUCCESS, data);
                }
                else
                {
                    return ErrorUtility<z_region>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.DATA_NOT_FOUND, data, Message.DATA_NOT_FOUND);
                }
            }
            catch (Exception ex)
            {
                return ErrorUtility<z_region>.GetErrorMessage(ErrorCode.ERROR, SubCode.INTERNAL_ERROR, Message.ERROR, ex.Message);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public OutputModels<List<z_region>> GetRegionList()
        {
            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new RegionService(conn);
                List<z_region> data = _service.RegionAll();

                if (data != null)
                {
                    return ErrorUtility<List<z_region>>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.SUCCESS, data);
                }
                else
                {
                    return ErrorUtility<List<z_region>>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.DATA_NOT_FOUND, data, Message.DATA_NOT_FOUND);
                }
            }
            catch (Exception ex)
            {
                return ErrorUtility<List<z_region>>.GetErrorMessage(ErrorCode.ERROR, SubCode.INTERNAL_ERROR, Message.ERROR, ex.Message);
            }

        }
    }
}