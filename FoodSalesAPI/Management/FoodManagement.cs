﻿using FoodSalesAPI.DBConnector;
using FoodSalesAPI.Models;
using FoodSalesAPI.Services;
using FoodSalesAPI.Utilitys;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FoodSalesAPI.Management
{
    public class FoodManagement    
    {
        #region Singleton
        private FoodService _service;
        private static readonly Lazy<FoodManagement> instance = new Lazy<FoodManagement>(() => Activator.CreateInstance<FoodManagement>());
        public static FoodManagement I
        {
            get
            {
                return instance.Value;
            }
        }
        public FoodManagement()
        {
        }
        #endregion

        /// <summary>
        /// Food By Id Management
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public OutputModels<m_food> FoodById(int id)
        {
            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new FoodService(conn);
                m_food data = _service.FoodById(id);
               
                if (data != null)
                {
                    return ErrorUtility<m_food>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.SUCCESS, data);
                }
                else 
                {
                    return ErrorUtility<m_food>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.DATA_NOT_FOUND, data, Message.DATA_NOT_FOUND);
                }
            }
            catch (Exception ex)
            {
                return ErrorUtility<m_food>.GetErrorMessage(ErrorCode.ERROR, SubCode.INTERNAL_ERROR, Message.ERROR, ex.Message);
            }

        }

        /// <summary>
        /// Get Food List Management
        /// </summary>
        /// <returns></returns>
        public OutputModels<List<m_food>> GetFoodList()
        {
            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new FoodService(conn);
                List<m_food> m_Foods = _service.GetFoodList();

                if (m_Foods != null)
                {
                    return ErrorUtility<List<m_food>>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.SUCCESS, m_Foods);
                }
                else
                {
                    return ErrorUtility<List<m_food>>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.DATA_NOT_FOUND, m_Foods, Message.DATA_NOT_FOUND);
                }
            }
            catch (Exception ex)
            {
                return ErrorUtility<List<m_food>>.GetErrorMessage(ErrorCode.ERROR, SubCode.INTERNAL_ERROR, Message.ERROR, ex.Message);
            }

        }

        /// <summary>
        /// delete Food By Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="inactive"></param>
        /// <returns></returns>
        public OutputModels<bool> deleteFoodById(int id, bool inactive)
        {
            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new FoodService(conn);
                bool data = _service.deleteFoodById(id, inactive);

                if (data == true)
                {
                    return ErrorUtility<bool>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.SUCCESS, data);
                }
                else
                {
                    return ErrorUtility<bool>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.DELETE_ERROR, data, Message.DELETE_ERROR);
                }
            }
            catch (Exception ex)
            {
                return ErrorUtility<bool>.GetErrorMessage(ErrorCode.ERROR, SubCode.INTERNAL_ERROR, Message.ERROR, ex.Message);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public OutputModels<bool> FoodAdd(FoodModelsIn req)
        {
            try
            {
                if (req == null)
                {
                    return ErrorUtility<bool>.GetSuccessMessage(ErrorCode.ERROR, SubCode.DATA_INPUT_ERROR, false, Message.DATA_INPUT_ERROR);
                }

                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new FoodService(conn);

                m_food m_Food = new m_food()
                {
                    Quantity = req.Quantity,
                    Category = req.Category,
                    City = req.City,
                    foodID = _service.GetMaxFoodId(),
                    inactive = false,
                    OrderDate = DateTime.Now,
                    Product = req.Product,
                    Region = req.Region,    
                    TotalPrice = req.TotalPrice,
                    UnitPrice = req.UnitPrice
                };

                bool data = _service.FoodAdd(m_Food);

                if (data == true)
                {
                    return ErrorUtility<bool>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.SUCCESS, data);
                }
                else
                {
                    return ErrorUtility<bool>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.INSERT_DATA_ERROR, data, Message.INSERT_DATA_ERROR);
                }
            }
            catch (Exception ex)
            {
                return ErrorUtility<bool>.GetErrorMessage(ErrorCode.ERROR, SubCode.INTERNAL_ERROR, Message.ERROR, ex.Message);
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="req"></param>
        /// <returns></returns>
        public OutputModels<bool> FoodEdit(FoodeditModelsIn req)
        {
            try
            {
                if (req == null)
                {
                    return ErrorUtility<bool>.GetSuccessMessage(ErrorCode.ERROR, SubCode.DATA_INPUT_ERROR, false, Message.DATA_INPUT_ERROR);
                }

                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new FoodService(conn);

                var infoOLD = _service.FoodById(req.foodID);
                if (infoOLD == null)
                {
                    return ErrorUtility<bool>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.INSERT_DATA_ERROR, false, Message.INSERT_DATA_ERROR);
                }

                m_food m_Food = new m_food()
                {
                    Quantity = req.Quantity,
                    Category = req.Category,
                    City = req.City,
                    foodID = infoOLD.foodID,
                    inactive = req.inactive,
                    OrderDate = infoOLD.OrderDate,
                    Product = req.Product,
                    Region = req.Region,
                    TotalPrice = req.TotalPrice,
                    UnitPrice = req.UnitPrice,
                    editDateTime = DateTime.Now,
                    _id = infoOLD._id
                };

                bool data = _service.FoodEdit(m_Food);

                if (data == true)
                {
                    return ErrorUtility<bool>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.SUCCESS, data);
                }
                else
                {
                    return ErrorUtility<bool>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.INSERT_DATA_ERROR, data, Message.INSERT_DATA_ERROR);
                }
            }
            catch (Exception ex)
            {
                return ErrorUtility<bool>.GetErrorMessage(ErrorCode.ERROR, SubCode.INTERNAL_ERROR, Message.ERROR, ex.Message);
            }

        }



        public OutputModels<List<m_food>> PostFoodSearch(FoodSearchModelsIn req)
        {
            try
            {
                IMongoDatabase conn = MongoConnector.I().DBConnect;
                _service = new FoodService(conn);
                List<m_food> m_Foods = _service.PostFoodSearch(req);

                if (m_Foods != null)
                {
                    return ErrorUtility<List<m_food>>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.SUCCESS, m_Foods);
                }
                else
                {
                    return ErrorUtility<List<m_food>>.GetSuccessMessage(ErrorCode.SUCCESS, SubCode.DATA_NOT_FOUND, m_Foods, Message.DATA_NOT_FOUND);
                }
            }
            catch (Exception ex)
            {
                return ErrorUtility<List<m_food>>.GetErrorMessage(ErrorCode.ERROR, SubCode.INTERNAL_ERROR, Message.ERROR, ex.Message);
            }

        }

    }
}