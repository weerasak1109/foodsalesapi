﻿using FoodSalesAPI.Models;
using FoodSalesAPI.Models.Mongo;
using Grpc.Core;
using Microsoft.Ajax.Utilities;
using Microsoft.SqlServer.Server;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Web;
using static MongoDB.Driver.WriteConcern;

namespace FoodSalesAPI.Services
{
    public class FoodService : MongoSystemService   
    {
        /// <summary>
        ///  logic constructor
        /// </summary>
        /// <param name="conn"></param>
        public FoodService(IMongoDatabase conn)
        {
            this._mongodb = conn;
        }

        /// <summary>
        /// Food By Id Service
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public m_food FoodById(int id)
        {
            try
            {
                var Con = this._mongodb.GetCollection<m_food>(typeof(m_food).Name);
                var info_food = Con.Find(t => t.foodID == id && t.inactive == false).FirstOrDefault();
                return info_food;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
                ex);
            }
        }

        /// <summary>
        ///  Insert Food Service
        /// </summary>
        /// <param name="lsit"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public bool InsertFood(List<m_food> lsit)
        {
            bool res = false;
            try
            {
                var Con = this._mongodb.GetCollection<m_food>(typeof(m_food).Name);
                Con.DeleteMany(e => e.foodID != null);
                Con.InsertMany(lsit);
                res = true;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
                ex);
            }
            return res;
        }

        /// <summary>
        /// Get Food List Service
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public List<m_food> GetFoodList()
        {
            try
            {
                var Con = this._mongodb.GetCollection<m_food>(typeof(m_food).Name);
                var foods = Con.Find(t =>t.inactive == false).SortBy(e => e.Product).ToList();
                return foods;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
                ex);
            }
        }


        /// <summary>
        /// delete Food By Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="inactive"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public bool deleteFoodById(int id, bool inactive)
        {
            bool res = false;
            try
            {
                var Con = this._mongodb.GetCollection<m_food>(typeof(m_food).Name);
                m_food cou = Con.Find(u => u.foodID == id).SingleOrDefault();
                if (cou != null)
                {
                    var query = Builders<m_food>.Filter.Eq(f => f._id, cou._id);
                    var update = Builders<m_food>.Update.Set(o => o.inactive, inactive);
                    var opts = new FindOneAndUpdateOptions<m_food>()
                    {
                        ReturnDocument = ReturnDocument.After
                    };
                    var auto_inc = Con.FindOneAndUpdate(query, update, opts);
                    res = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
                ex);
            }
            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public int GetMaxFoodId()
        {
            int res = 1;
            try
            {
                var Con = this._mongodb.GetCollection<m_food>(typeof(m_food).Name);
                var List = Con.Find(t => t.Product != null).SortBy(e => e.foodID).ToList();
                if (List != null)
                {
                    res = List.LastOrDefault().foodID + 1;
                }
                
                return res;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
                ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lsit"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public bool FoodAdd(m_food lsit)
        {
            bool res = false;
            try
            {
                var Con = this._mongodb.GetCollection<m_food>(typeof(m_food).Name);
                Con.InsertOne(lsit);
                res = true;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
                ex);
            }
            return res;
        }

        public bool FoodEdit(m_food food)
        {
            bool res = false;
            try
            {
                var Con = this._mongodb.GetCollection<m_food>(typeof(m_food).Name);
                var filter = Builders<m_food>.Filter.Eq(f => f._id, food._id);
                var result = Con.ReplaceOneAsync(filter, food);
                res = true;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
                ex);
            }
            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lsit"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public List<m_food> PostFoodSearch(FoodSearchModelsIn req)
        {
            try
            {
                var Con = this._mongodb.GetCollection<m_food>(typeof(m_food).Name);
                var foods = Con.Find(t => t.inactive == false).SortBy(e => e.Product).ToList();

                if (req.ProductNameSearch != null && req.ProductNameSearch != "")
                {
                    foods = foods.Where(r => r.Product.Contains(req.ProductNameSearch.Trim())).ToList();
                }
                if (req.SearchCategory != null && req.SearchCategory != "")
                {
                    foods = foods.Where(r => r.Category.Contains(req.SearchCategory.Trim())).ToList();
                }
                if (req.SearchCity != null && req.SearchCity != "")
                {
                    foods = foods.Where(r => r.City.Contains(req.SearchCity.Trim())).ToList();
                }
                if (req.SearchRegion != null && req.SearchRegion != "")
                {
                    foods = foods.Where(r => r.Region.Contains(req.SearchRegion.Trim())).ToList();
                }
                if (req.OrderDateStart != null && req.OrderDateStart != "")
                {
                    var dateTime = DateTime.ParseExact(req.OrderDateStart.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    foods = foods.Where(r => r.OrderDate >= dateTime).ToList();
                }
                if (req.OrderDateEnd != null && req.OrderDateEnd != "")
                {
                    var dateTime = DateTime.ParseExact(req.OrderDateEnd.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                    foods = foods.Where(r => r.OrderDate <= Convert.ToDateTime(dateTime)).ToList();
                }

                return foods;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
                ex);
            }
        }

    }
}