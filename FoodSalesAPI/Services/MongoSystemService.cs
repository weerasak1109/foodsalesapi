﻿using FoodSalesAPI.Models.Mongo;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Diagnostics.PerformanceData;
using System.Linq;
using System.Web;

namespace FoodSalesAPI.Services
{
    public class MongoSystemService
    {
        /// <summary>
        /// เชื่อมต่อฐานข้อมูล
        /// </summary>
        public IMongoDatabase _mongodb { get; set; }

        /// <summary>
        /// เรียก sequence name
        /// </summary>
        /// <param name="sequence_name">sequence name</param>
        public Int64 GetIncrement(string sequence_name)
        {
            try
            {

                var colle = _mongodb.GetCollection<counters>(typeof(counters).Name);
                counters auto_inc = new counters();
                List<counters> cou = colle.Find(u => u.t_name == sequence_name).ToList();
                if (cou.Count == 1)
                {
                    counters info_counters = cou.First();
                    info_counters.seq += 1;
                    var query = Builders<counters>.Filter.Eq(f => f.t_name, sequence_name);
                    var update = Builders<counters>.Update.Set(o => o.seq, info_counters.seq);
                    var opts = new FindOneAndUpdateOptions<counters>()
                    {
                        ReturnDocument = ReturnDocument.After
                    };
                    auto_inc = colle.FindOneAndUpdate(query, update, opts);
                }
                else
                {
                    auto_inc = new counters()
                    {
                        seq = 1,
                        t_name = sequence_name
                    };
                    colle.InsertOne(auto_inc);
                }
                if (auto_inc == null) throw new Exception("Not Increment");
                return auto_inc.seq;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}