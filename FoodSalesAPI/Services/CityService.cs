﻿using FoodSalesAPI.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FoodSalesAPI.Services
{
    public class CityService : MongoSystemService
    {
        /// <summary>
        ///  logic constructor
        /// </summary>
        /// <param name="conn"></param>
        public CityService(IMongoDatabase conn)
        {
            this._mongodb = conn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lsit"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public bool InsertCity(List<z_city> lsit)
        {
            bool res = false;
            try
            {
                var Con = this._mongodb.GetCollection<z_city>(typeof(z_city).Name);
                Con.DeleteMany(e => e.City != null);
                Con.InsertMany(lsit);
                res = true;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
                ex);
            }
            return res;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="iName"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public z_city CityByName(string iName)
        {
            try
            {
                var Con = this._mongodb.GetCollection<z_city>(typeof(z_city).Name);
                var info = Con.Find(t => t.City == iName && t.inactive == false).FirstOrDefault();
                return info;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
                ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public List<z_city> CityAll()
        {
            try
            {
                var Con = this._mongodb.GetCollection<z_city>(typeof(z_city).Name);
                var list = Con.Find(t => t.inactive == false).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
                ex);
            }
        }
    }
}