﻿using FoodSalesAPI.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FoodSalesAPI.Services
{
    public class CategoryService : MongoSystemService
    {
        /// <summary>
        ///  logic constructor
        /// </summary>
        /// <param name="conn"></param>
        public CategoryService(IMongoDatabase conn)
        {
            this._mongodb = conn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="lsit"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public bool InsertCategory(List<z_category> lsit)
        {
            bool res = false;
            try
            {
                var Con = this._mongodb.GetCollection<z_category>(typeof(z_category).Name);
                Con.DeleteMany(e => e.Category != null);
                Con.InsertMany(lsit);
                res = true;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
                ex);
            }
            return res;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="iName"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public z_category CategoryByName(string iName)
        {
            try
            {
                var Con = this._mongodb.GetCollection<z_category>(typeof(z_category).Name);
                var info = Con.Find(t => t.Category == iName && t.inactive == false).FirstOrDefault();
                return info;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
                ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        public List<z_category> CategoryAll()
        {
            try
            {
                var Con = this._mongodb.GetCollection<z_category>(typeof(z_category).Name);
                var list = Con.Find(t => t.inactive == false).ToList();
                return list;
            }
            catch (Exception ex)
            {
                throw new Exception(
                  string.Format("c'{0}'-f'{1}'-m'{2}'-s'{3}-t{4}'",
                  this.GetType().Name,
                  MethodBase.GetCurrentMethod().Name,
                  ex.Message,
                  ex.Source,
                  ex.StackTrace
                  ),
                ex);
            }
        }

    }
}