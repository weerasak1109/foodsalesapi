﻿using FoodSalesAPI.Models.Mongo;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodSalesAPI.Models
{
    public class m_food : DataConstEntity
    {
        /// <summary>
        /// รหัสสินค้า
        /// </summary>
        public int foodID { get; set; }
        /// <summary>
        /// วันที่
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime OrderDate { get; set; }
        /// <summary>
        /// ภูมิภาค
        /// </summary>
        public string Region { get; set; }
        /// <summary>
        /// เมือง
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// Category
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// ชื่อสินค้า
        /// </summary>
        public string Product { get; set; }
        /// <summary>
        /// จำนวน
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// ราคาต่อหน่วย
        /// </summary>
        public double UnitPrice { get; set; }
        /// <summary>
        /// ราคารวม
        /// </summary>
        public double TotalPrice { get; set; }
    }

    public class food_ex
    {
        public DateTime OrderDate { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
        public string Category { get; set; }
        public string Product { get; set; }
        public double Quantity { get; set; }
        public double UnitPrice { get; set; }
        public double TotalPrice { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class FoodModelsIn
    {
        /// <summary>
        /// ภูมิภาค
        /// </summary>
        public string Region { get; set; }
        /// <summary>
        /// เมือง
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// Category
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// ชื่อสินค้า
        /// </summary>
        public string Product { get; set; }
        /// <summary>
        /// จำนวน
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// ราคาต่อหน่วย
        /// </summary>
        public double UnitPrice { get; set; }
        /// <summary>
        /// ราคารวม
        /// </summary>
        public double TotalPrice { get; set; }
    }

    public class FoodeditModelsIn : m_food
    {
        
    }


    public class FoodSearchModelsIn
    {
        public string SearchCategory { get; set; }
        public string SearchCity { get; set; }
        public string SearchRegion { get; set; }
        public string ProductNameSearch { get; set; }
        public string OrderDateStart { get; set; }
        public string OrderDateEnd { get; set; }
    }

}