﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodSalesAPI.Models.Mongo
{

    /// <summary>
    /// 
    /// </summary>
    [BsonIgnoreExtraElements]
    public class counters
    {
        public string t_name { get; set; }
        public long seq { get; set; }
    }

    /// <summary>
    /// ข้อมูลพื้นฐาน
    /// </summary>
    public abstract class DataBaseEntity
    {
        /// <summary>
        /// รหัส
        /// </summary>
        public ObjectId _id { get; set; }
    }

    /// <summary>
    /// ข้อมูลคงที่
    /// </summary>
    public abstract class DataConstEntity : DataBaseEntity
    {
        /// <summary>
        /// ไม่ใช้งาน
        /// </summary>
        public Boolean inactive { get; set; } = false;

        /// <summary>
        /// ไม่ใช้งาน
        /// </summary>
        [BsonDateTimeOptions(Kind = DateTimeKind.Local)]
        public DateTime? editDateTime { get; set; } 

    }
}