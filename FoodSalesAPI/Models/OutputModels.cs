﻿using FoodSalesAPI.Utilitys;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodSalesAPI.Models
{
    /// <summary>
    /// ข้อมูลผิดพลาด
    /// </summary>
    public class OutputModels<TValue>
    {
        /// <summary>
		/// รหัสข้อผิดพลาด
		/// </summary>
		public int error_code { get; set; } = ErrorCode.ERROR;
        /// <summary>
        /// รหัสย่อยข้อผิดพลาด
        /// </summary>
        public int sub_code { get; set; } = SubCode.DATA_NOT_FOUND;
        /// <summary>
        /// หัวเรื่อง
        /// </summary>
        public string title { get; set; } = "";
        /// <summary>
        /// ข้อความ ข้อผิดพลาด
        /// </summary>
        public string message { get; set; }
        /// <summary>
        /// ข้อมูล
        /// </summary>
        public TValue data { get; set; } 
    }
}